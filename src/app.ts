import express from 'express';

const app = express();

const obtenerResultadoSegunParametros = (arrayDeParametros) => {
    if (arrayDeParametros.length) {
        let resultado = "La lista de parametros es:</br>";
        arrayDeParametros.forEach(parametro => resultado += `${parametro[0]}: ${parametro[1]}</br>`);
        return {resultado};
    }
    return "No hay parametros para mostrar";

}

const obtenerArrayDeParametros = (parametros) => {
    return Object.entries(parametros)
}

app.get("/", (req, res) => {
    const arrayDeParametros = obtenerArrayDeParametros(req.query);
    const resultado = obtenerResultadoSegunParametros(arrayDeParametros);
    res.send(resultado);
})


app.listen('3000', () => console.log("Servidor iniciado. A disfrutar don"))